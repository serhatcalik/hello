
# BASE
FROM node:fermium-alpine3.13 AS base
RUN apk add nodejs tini
WORKDIR /root/hello

ENTRYPOINT ["/sbin/tini", "--"]

COPY package.json .


# needs

FROM base AS dependencies 

RUN npm set progress=false && npm config set depth 0
RUN npm install --only=production 
# copy production node_modules aside
RUN cp -R node_modules prod_node_modules
# install ALL node_modules, including 'devDependencies'
RUN npm install

# TEST

FROM dependencies AS test
COPY . .
RUN  npm run test


# Release
FROM base AS release
COPY --from=dependencies /root/hello/prod_node_modules ./node_modules

COPY . .

CMD npm run start

# Multi stage for bigger projects.
